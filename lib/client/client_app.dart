import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/chat_screen.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';

import '../sl/getIt.dart';
import 'bloc/client_bloc.dart';
import 'bloc/client_state.dart';
import 'bloc/client_event.dart';
import 'data/client_repository.dart';
import 'data/client_user_repository.dart';

class SupChatApp extends StatefulWidget {
  final Map<String, String> customUserDetails;

  const SupChatApp({Key key, this.customUserDetails})
      : super(key: key);

  @override
  _SupChatAppState createState() => _SupChatAppState();
}

class _SupChatAppState extends State<SupChatApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            body: SafeArea(
                child: BlocConsumer<ClientBloc, ClientState>(
                    cubit: ClientBloc(
                      ClientRepository(getIt<ChatConfig>()),
                      ClientUserRepository(getIt<ChatConfig>()),
                      getIt<ChatConfig>(),
                      getIt<ChatRepository>(),
                      widget.customUserDetails,
                    )..add(ClientEnterEvent(getIt<ChatConfig>())),
                    listenWhen: (previous, current) =>
                        current is ClientEnteredState,
                    listener: (BuildContext context, ClientState state) {
                      _navigateToChat(context, state as ClientEnteredState);
                    },
                    builder: (BuildContext context, ClientState state) =>
                        Center(
                          child: CircularProgressIndicator(),
                        )))));
  }

  void _navigateToChat(BuildContext context, ClientEnteredState state) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (_) => ChatScreen(
                chatId: state.clientId,
                userId: state.clientId,
                userRepository: getIt<UserRepository>(),
                chatRepository: getIt<ChatRepository>(),
                chatConfig: getIt<ChatConfig>(),
              ),
      ),
    );
  }
}
