
abstract class ClientState {
}

class InitialClientState extends ClientState {}

class LoadingState extends ClientState {}

class ClientEnteredState extends ClientState {
  final String customerId;
  final String clientId;

  ClientEnteredState(this.customerId, this.clientId);
}
