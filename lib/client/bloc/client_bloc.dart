import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/model/chat_user.dart';
import '../data/client_repository.dart';
import '../data/client_user_repository.dart';
import './bloc.dart';

class ClientBloc extends Bloc<ClientEvent, ClientState> {
  final ClientRepository _clientRepository;
  final ClientUserRepository _userRepository;
  final ChatRepository _clientChatRepository;
  final ChatConfig _config;
  final Map<String, String> customUserDetails;

  ClientBloc(this._clientRepository, this._userRepository, this._config, this._clientChatRepository,
      this.customUserDetails) : super(InitialClientState());

  @override
  Stream<ClientState> mapEventToState(
    ClientEvent event,
  ) async* {
    if (event is ClientEnterEvent) {
      yield* _mapClientEnterToState();
    }
  }

  Stream<ClientState> _mapClientEnterToState() async* {
    yield LoadingState();

    final localUser = await _userRepository.getChatUser();
    if (localUser != ChatUser.empty) {
      //send request for details update
      yield ClientEnteredState(_config.customerId, localUser.id);
    } else {
      final newUserId = await _clientRepository.createAnonymousClient(this.customUserDetails);//with user details
      await _userRepository.saveUser(newUserId);
      print('new user id: $newUserId');
      yield ClientEnteredState(_config.customerId, newUserId);
    }
    await _clientChatRepository.initialize();
  }
}
