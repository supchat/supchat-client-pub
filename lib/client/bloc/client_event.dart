import 'package:supchat_chat/chat_config.dart';

abstract class ClientEvent {}

class ClientEnterEvent extends ClientEvent{
  final ChatConfig config;

  ClientEnterEvent(this.config);
}
