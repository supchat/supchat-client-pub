
class Timestamp {

  final int seconds;
  final int _nanoseconds;

	Timestamp.fromJsonMap(Map<String, dynamic> map): 
		seconds = map["_seconds"],
		_nanoseconds = map["_nanoseconds"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['_seconds'] = seconds;
		data['_nanoseconds'] = _nanoseconds;
		return data;
	}
}
