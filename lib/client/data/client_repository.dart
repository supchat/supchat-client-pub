import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supchat_chat/chat_config.dart';

const String API_URL = 'https://supchat.ai/back';
const String SOCKET_URL = 'https://supchat.ai';

class ClientRepository {
  final ChatConfig _config;

  ClientRepository(this._config);

  Future<String> createAnonymousClient(
      [Map<String, String> customUserDetails]) async {
    final response = await http.post(
        Uri.parse('$API_URL/create-client?customer_id=${_config.customerId}'),
        body: customUserDetails);

    String userId = response.body;
    return userId;
  }

  Future<dynamic> updateClientDetails(Map<String, String> customUserDetails) {
    if (customUserDetails == null || customUserDetails.isEmpty)
      return Future.error("empty details");
  }
}
