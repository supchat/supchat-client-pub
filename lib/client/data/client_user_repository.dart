import 'package:shared_preferences/shared_preferences.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';
import 'package:supchat_chat/chat/model/chat_user.dart';

class ClientUserRepository extends UserRepository {
  final ChatConfig _chatConfig;

  ClientUserRepository(this._chatConfig);

  @override
  Future<ChatUser> getChatUser() async {
    final prefs = await SharedPreferences.getInstance();
    final localUserId = prefs.getString('id');
    if (localUserId == null) {
      return ChatUser.empty;
    }
    return ChatUser(localUserId, null, null, null, null);
  }

  Future<void> saveUser(String userId) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('id', userId);
  }
}
