import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:rxdart/rxdart.dart';
import 'package:path/path.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/model/chat_info.dart';
import 'package:supchat_chat/chat/model/message.dart';
import 'package:supchat_client/client/data/client_user_repository.dart';

import 'client_repository.dart';
import 'MessageResponse.dart';

class ClientChatRepository extends ChatRepository {
  final ChatConfig _config;

  final ClientUserRepository _userRepository;
  final Logger _logger;

  IO.Socket _socket;

  ClientChatRepository(this._config, this._userRepository, this._logger);

  BehaviorSubject<Message> _newMessages = BehaviorSubject<Message>();
  BehaviorSubject<ChatInfo> _chatInfo = BehaviorSubject<ChatInfo>();

  Future<dynamic> initialize() async {
    final chatUser = await _userRepository.getChatUser();
    _socket = IO.io(
        '$SOCKET_URL?customer_id=${_config.customerId}&client_id=${chatUser.id}',
        <String, dynamic>{
          'transports': ['websocket']
        });
    _socket.on('connect', (data) => print('socket connected $data'));
    _socket.on('new_message', (data) => _mapNewMessage(data, chatUser.id));
    _socket.on('chat_info', (data) => _mapChatInfo(data, chatUser.id));
  }

  _mapNewMessage(Map<String, dynamic> data, String userId) {
    print("ClientChatRepository message received $data");
    final message = MessageResponse.fromJsonMap(data).toMessage(userId);
    _newMessages.add(message);
  }

  _mapChatInfo(data, String userId) {
    var online = data['online'];
    var lastChanged = online['last_changed']['_seconds'];
    final chatInfo = ChatInfo(
        name: data['nickname'],
        id: data['id'],
        pictureUrl: data['photoUrl'],
        online: Online(
            online: online['state'],
            lastUpdate:
                DateTime.fromMillisecondsSinceEpoch(lastChanged * 1000)));
    print(data);
    _chatInfo.add(chatInfo);
  }

  @override
  Stream<List<Message>> onNewMessages(String chatId) {
    return _newMessages.stream.map((message) => [message]);
  }

  @override
  void close() {
    _socket.close();
    _newMessages.close();
    _chatInfo.close();
  }

  @override
  Future<List<Message>> getChatHistory(String chatId) async {
    final user = await _userRepository.getChatUser();

    return http
        .get(Uri.parse(
            '$API_URL/getchathistory?customer_id=${_config.customerId}&client_id=$chatId'))
        .then((value) {
      return (jsonDecode(value.body) as List<dynamic>)
          .map((messageBody) =>
              MessageResponse.fromJsonMap(messageBody).toMessage(user.id))
          .toList();
    });
  }

  @override
  Future<dynamic> sendMessage(String chatId, Message message) async {
    final user = await _userRepository.getChatUser();
    final url =
        '$API_URL/sendMessage?customer_id=${_config.customerId}&client_id=${user.id}';
    return http.post(Uri.parse(url), body: {
      'content': message.content,
      'idFrom': message.idFrom,
      'type': message.type.toString(),
      'chatId': message.idFrom
    });
  }

  @override
  Stream<ChatInfo> loadChatInfo(String chatId) {
    return _chatInfo.stream;
  }

  @override
  Future<dynamic> markChatSeen(String chatId) {
    //did by backend
    return Future.error("Not implemented");
  }

  @override
  Stream updateOnline(String id, {String chatMateId}) {
    //did by backend
    return Stream.empty();
  }

  @override
  Future<String> uploadFile(
    dynamic data,
  ) async {
    try {
      final bytes = (data is File)
          ? data.readAsBytesSync()
          : Uri.parse(data).data.contentAsBytes();
      final fileName =
          (data is File) ? basename(data.path) : 'image-${DateTime.now()}';
      print(
          "uploadFile bytes: ${bytes.length} fileName: $fileName data: ${data}");

      final request =
          new http.MultipartRequest("POST", Uri.parse('$API_URL/sendImage'));
      request.files.add(
          http.MultipartFile.fromBytes('image', bytes, filename: "fileName"));
      request.fields['customer_id'] = _config.customerId;

      final response = await request.send();

      var body = await response.stream.transform(utf8.decoder).first;
      print('image uploaded $body');
      return body;
    } catch (e) {
      print(e);
    }
    return 'error';
  }
}
