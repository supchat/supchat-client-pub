import 'package:supchat_chat/chat/model/message.dart';
import 'timestamp.dart';

class MessageResponse {
  final Timestamp timestamp;
  final String content;
  final int type;
  final String idFrom;
  final String idTo;

  MessageResponse.fromJsonMap(Map<String, dynamic> map)
      : timestamp = Timestamp.fromJsonMap(map["timestamp"]),
        content = map["content"],
        type = map["type"],
        idFrom = map["idFrom"],
        idTo = map["idTo"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['timestamp'] = timestamp == null ? null : timestamp.toJson();
    data['content'] = content;
    data['type'] = type;
    data['idFrom'] = idFrom;
    data['idTo'] = idTo;
    return data;
  }

  Message toMessage(String userId) {
    return Message(
        content: this.content,
        idFrom: this.idFrom,
        time: DateTime.fromMillisecondsSinceEpoch(this.timestamp.seconds),
        isLast: false,
        isMy: userId == this.idFrom,
        type: this.type);
  }
}
