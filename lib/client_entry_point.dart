import 'package:flutter/material.dart';
import 'package:supchat_chat/chat_config.dart';
import 'args/shared.dart';
import 'client/client_app.dart';
import 'sl/getIt.dart';

void main() {
  _startChat();
}

void _startChat({String apiKey, Map<String, String> customUserDetails}) {
  String customerId;
  try {
    customerId = getInitialArgs();
  } catch (e) {
    customerId = apiKey;
  }
  if (customerId == null) {
    throw "No api key provided";
  }
  var config = LibraryConfig(customerId: customerId);
  initSupChat(config);
  runApp(SupChatApp(
    customUserDetails: customUserDetails,
  ));
}

class LibraryConfig extends ChatConfig {
  LibraryConfig({@required customerId}) : super(customerId: customerId, isLibrary: true);
}
