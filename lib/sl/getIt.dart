import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';

import '../client/data/client_user_repository.dart';
import '../client/data/client_chat_repository.dart';

final getIt = GetIt.instance;

var isInit = false;

initSupChat(ChatConfig config) {
  if (isInit) return;
  getIt.registerLazySingleton<ChatRepository>(() =>
      ClientChatRepository(getIt<ChatConfig>(), getIt<UserRepository>(), getIt<Logger>()));

  getIt.registerSingleton<ChatConfig>(config);
  getIt.registerLazySingleton<UserRepository>(
      () => ClientUserRepository(getIt<ChatConfig>()));

  getIt.registerLazySingleton<Logger>(() => Logger());
  isInit = true;
}
