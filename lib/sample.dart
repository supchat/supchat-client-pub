import 'package:flutter/material.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_client/client/client_app.dart';
import 'package:supchat_client/client_entry_point.dart';
import 'package:supchat_client/sl/getIt.dart';

void main() {
  initSupChat(LibraryConfig(customerId: "SrSvgXEi2pQSp4DpxiEAR5Y4Vrs2"));
  runApp(SupChatSampleApp());
}

class SupChatSampleApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        floatingActionButton: Builder(
          builder: (context) => FloatingActionButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => SupChatApp(customUserDetails: {"testKey": "textValue"})));
              },
              child: Text("CHAT")),
        ),
      ),
    );
  }
}
